#!/bin/bash

Summ="1  -截取所有屏幕\n2 -截取当前屏幕\n3 -截取当前聚焦窗口\n4 -截取选区\n5 -截取选区并保存至剪贴板"

MODE=$(echo $(echo -e $Summ | wofi --dmenu -p '截图选项' )|sed -r 's/(.{1}).*/\1/')

PicAdr="$HOME/Pictures/Screenshots/$(date +'%s_grim.png')"

case $MODE in 
	1)
		grim $PicAdr
		;;
	2)
		sleep 2
		grim -o $(hyprctl -j monitors  | jq -j '.[] | select(.focused).name') $PicAdr
		;;
	3)
		sleep 2
		grim -g "$( hyprctl -j activewindow  | jq -j '"\(.at[0]),\(.at[1]) \(.size[0])x\(.size[1])"')" $PicAdr
		;;
	4)
		grim -g "$(slurp)" $PicAdr
		;;
	5)
		grim -g "$(slurp -d)" - | wl-copy
		;;
esac

