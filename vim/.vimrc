call plug#begin()
" let g:plug_url_format = 'git@github.com:%s.git'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'jiangmiao/auto-pairs'
Plug 'godlygeek/tabular'
Plug 'plasticboy/vim-markdown'
Plug 'lervag/vimtex'
Plug 'elkowar/yuck.vim'
" Plug 'preservim/nerdtree'
" Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'airblade/vim-gitgutter'
" Plug 'tpope/vim-fugitive'
Plug 'mhinz/vim-startify'
Plug 'SirVer/ultisnips'
Plug 'arcticicestudio/nord-vim'
" Plug 'AhmedAbdulrahman/vim-aylin'
" Plug 'AlessandroYorba/Sierra'
Plug 'sts10/vim-pink-moon'
" Plug 'beikome/cosme.vim'
" Plug 'hzchirs/vim-material'
Plug 'junegunn/seoul256.vim'
" Plug 'yuttie/hydrangea-vim'
" Plug 'KabbAmine/yowish.vim'
" Plug 'morhetz/gruvbox'
" if has('nvim')
"   Plug 'Shougo/defx.nvim', { 'do': ':UpdateRemotePlugins' }
" else
"   Plug 'Shougo/defx.nvim'
"   Plug 'roxma/nvim-yarp'
"   Plug 'roxma/vim-hug-neovim-rpc'
" endif
Plug 'kristijanhusak/defx-icons'
Plug 'ryanoasis/vim-devicons'
" Plug 'neoclide/coc.nvim', {'branch':'release'}
call plug#end()


" coc-snippets
" inoremap <silent><expr> <TAB>
"       \ pumvisible() ? coc#_select_confirm() :
"       \ coc#expandableOrJumpable() ? "\<C-r>=coc#rpc#request('doKeymap', ['snippets-expand-jump',''])\<CR>" :
"       \ <SID>check_back_space() ? "\<TAB>" :
"       \ coc#refresh()
" 
" function! s:check_back_space() abort
"   let col = col('.') - 1
"   return !col || getline('.')[col - 1]  =~# '\s'
" endfunction

" let g:coc_snippet_next = '<tab>'

" fcitx-vim
let g:ttimeoutlen = 100

set termguicolors " 真彩色输出

" set guifont=Sarasa\ Mono\ SC\ Nerd\ Light\ 9

" VIM-Airline 
set laststatus=2 " 总是显示状态栏 
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1 
let g:airline_theme='monochrome'

if !exists('g:airline_symbols')
let g:airline_symbols = {}
endif
let g:airline_left_sep = '▶'
let g:airline_left_alt_sep = '❯'
let g:airline_right_sep = '◀'
let g:airline_right_alt_sep = '❮'
let g:airline_symbols.linenr = ' '
let g:airline_symbols.branch = '⎇'
let g:airline_symbols.maxlinenr = ' '
let g:airline_symbols.notexists = 'Ɇ'
let g:airline_symbols.whitespace = 'Ξ'
let g:airline_symbols.colnr = ' '


set fileencodings=utf-8,chinese,latin-1
set termencoding=utf-8
set encoding=utf-8

let g:vim_markdown_math = 1

" UltiSnips

let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<s-tab>"
let g:UltiSnipsSnippetDirectories=[$HOME.'/.vim/UltiSnips']


let g:vim_markdown_folding_disabled = 1
let g:vim_markdown_math = 1

filetype plugin on
syntax on

set conceallevel=2

" set scrolloff=8

set nu
colorscheme pink-moon 

highlight Normal guibg=NONE ctermbg=None

inoremap <c-d> <end>
inoremap <c-a> <home>
inoremap <c-c> <esc>

" YCM 设置
let g:ycm_key_invoke_completion = '<C-z>'
let g:ycm_key_list_select_completion = '<C-n>'
let g:ycm_key_list_previous_completion = '<C-p>'
let g:SuperTabDefaultCompletionType = '<C-n>'


let g:gitgutter_set_sign_backgrounds = 0

highlight SignColumn guibg=NONE ctermbg=None

highlight GitGutterAdd    guibg=NONE ctermbg=None  
highlight GitGutterChange guibg=NONE ctermbg=None  
highlight GitGutterDelete guibg=NONE ctermbg=None  

highlight LineNr guibg=NONE
" defx设置

" nmap <silent> tt :Defx <cr>
" 
" call defx#custom#option('_', {
"       \ 'winwidth': 30,
"       \ 'split': 'vertical',
"       \ 'direction': 'topleft',
"       \ 'show_ignored_files': 0,
"       \ 'buffer_name': '',
"       \ 'toggle': 1,
"       \ 'resume': 1
"       \ })

" autocmd FileType defx call s:defx_my_settings()
" function! s:defx_my_settings() abort
"   " Define mappings
"   nnoremap <silent><buffer><expr> <CR>
"   \ defx#do_action('open')
"   nnoremap <silent><buffer><expr> c
"   \ defx#do_action('copy')
"   nnoremap <silent><buffer><expr> m
"   \ defx#do_action('move')
"   nnoremap <silent><buffer><expr> p
"   \ defx#do_action('paste')
"   nserve the peoplenoremap <silent><buffer><expr> l
"   \ defx#do_action('open')
"   nnoremap <silent><buffer><expr> E
"   \ defx#do_action('open', 'vsplit')
"   nnoremap <silent><buffer><expr> P
"   \ defx#do_action('preview')
"   nnoremap <silent><buffer><expr> o
"   \ defx#do_action('open_tree', 'toggle')
"   nnoremap <silent><buffer><expr> K
"   \ defx#do_action('new_directory')
"   nnoremap <silent><buffer><expr> N
"   \ defx#do_action('new_file')
"   nnoremap <silent><buffer><expr> M
"   \ defx#do_action('new_multiple_files')
"   nnoremap <silent><buffer><expr> C
"   \ defx#do_action('toggle_columns',
"   \                'mark:indent:icon:filename:type:size:time')
"   nnoremap <silent><buffer><expr> S
"   \ defx#do_action('toggle_sort', 'time')
"   nnoremap <silent><buffer><expr> d
"   \ defx#do_action('remove')
"   nnoremap <silent><buffer><expr> r
"   \ defx#do_action('rename')
"   nnoremap <silent><buffer><expr> !
"   \ defx#do_action('execute_command')
"   nnoremap <silent><buffer><expr> x
"   \ defx#do_action('execute_system')
"   nnoremap <silent><buffer><expr> yy
"   \ defx#do_action('yank_path')
"   nnoremap <silent><buffer><expr> .
"   \ defx#do_action('toggle_ignored_files')
"   nnoremap <silent><buffer><expr> ;
"   \ defx#do_action('repeat')
"   nnoremap <silent><buffer><expr> h
"   \ defx#do_action('cd', ['..'])
"   nnoremap <silent><buffer><expr> ~
"   \ defx#do_action('cd')
"   nnoremap <silent><buffer><expr> q
"   \ defx#do_action('quit')
"   nnoremap <silent><buffer><expr> <Space>
"   \ defx#do_action('toggle_select') . 'j'
"   nnoremap <silent><buffer><expr> *
"   \ defx#do_action('toggle_select_all')
"   nnoremap <silent><buffer><expr> j
"   \ line('.') == line('$') ? 'gg' : 'j'
"   nnoremap <silent><buffer><expr> k
"   \ line('.') == 1 ? 'G' : 'k'
"   nnoremap <silent><buffer><expr> <C-l>
"   \ defx#do_action('redraw')
"   nnoremap <silent><buffer><expr> <C-g>
"   \ defx#do_action('print')
"   nnoremap <silent><buffer><expr> cd
"   \ defx#do_action('change_vim_cwd')
" endfunction

let g:startify_custom_header = [
			\ '    ███████╗ ██████╗ ███╗   ██╗ ██████╗     ██╗     █████╗ ███╗   ███╗   ', 
			\ '    ██╔════╝██╔═══██╗████╗  ██║██╔════╝     ██║    ██╔══██╗████╗ ████║   ', 
			\ '    ███████╗██║   ██║██╔██╗ ██║██║  ███╗    ██║    ███████║██╔████╔██║   ', 
			\ '    ╚════██║██║   ██║██║╚██╗██║██║   ██║    ██║    ██╔══██║██║╚██╔╝██║   ', 
			\ '    ███████║╚██████╔╝██║ ╚████║╚██████╔╝    ██║    ██║  ██║██║ ╚═╝ ██║██╗', 
			\ '    ╚══════╝ ╚═════╝ ╚═╝  ╚═══╝ ╚═════╝     ╚═╝    ╚═╝  ╚═╝╚═╝     ╚═╝╚═╝', 
\ ]

if filereadable(expand('~/.vim/conf/rc/cus_front.vim'))
  exe 'source' '~/.vim/conf/rc/cus_front.vim'
endif
