#!/bin/bash

SCREEN="eDP-1"
WAYLANDINPUTR=("0:0:Elan_Touchscreen")
WAYLANDINPUTN=("1:1:AT_Translated_Set_2_keyboard"
	"1267:143:Elan_Touchpad"
	)


function enterTabletMode {
	for i in "${WAYLANDINPUTN[@]}"
    	do
        	swaymsg input "$i" events disabled
    	done
	notify-send "tablet on"
}


function exitTabletMode {
	for i in "${WAYLANDINPUTN[@]}"
    	do
        	swaymsg input "$i" events enabled 
    	done
	notify-send "tablet off"
}

function rotateScreenTo {
	case $1 in
		0|90|180|270)
			swaymsg "output $SCREEN transform $1"
			for i in "${WAYLANDINPUTR[@]}"
   				do
   				     swaymsg input "$i" map_to_output "$SCREEN"
   				done
		;;
	esac
}

case $1 in
	"ENTER")
		enterTabletMode
		;;
	"EXIT")
		exitTabletMode
		;;
	"ROTATETO")
		rotateScreenTo $2
		;;
	"ROTATE")
		case  $(swaymsg -t get_outputs | jq -r '.. | select(.name?=="eDP-1") | .transform') in
			normal)
				NOWPOSOTION=0
				;;
			90)
				NOWPOSOTION=90
				;;
			180)
				NOWPOSOTION=180
				;;
			270)
				NOWPOSOTION=270
				;;
		esac
		case $2 in
			90|180|270)
				ROTATEANGLE=$2
			#	echo $ROTATEANGLE
			#	echo $NOWPOSOTION
				SUMPOSITION=$(((NOWPOSOTION+ROTATEANGLE)%360))
			#	echo $SUMPOSITION
				rotateScreenTo $SUMPOSITION
				;;
		esac
		;;
esac


