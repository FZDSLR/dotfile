#!/bin/bash

Summ="1  截取所有屏幕\n2  截取当前屏幕\n3  截取当前聚焦窗口\n4  截取选区"

MODE=$(echo $(echo -e $Summ | wofi --dmenu -p '截图选项' )|sed -r 's/(.{1}).*/\1/')

PicAdr="$HOME/Pictures/Screenshots/$(date +'%s_grim.png')"

case $MODE in 
	1)
		grim $PicAdr
		;;
	2)
		sleep 2
		grim -o $(swaymsg -t get_outputs | jq -r '.[] | select(.focused) | .name') $PicAdr
		;;
	3)
		sleep 2
		grim -g "$(swaymsg -t get_tree | jq -j '.. | select(.type?) | select(.focused).rect | "\(.x),\(.y) \(.width)x\(.height)"')" $PicAdr
		;;
	4)
		grim -g "$(slurp)" $PicAdr
		;;
esac

