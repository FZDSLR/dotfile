#!/usr/bin/env bash

getvolume(){
	MUTE=$(pamixer --get-mute)
if [ "$MUTE" == "false" ];
then
  pamixer --get-volume | cut -d " " -f1;
else
  echo "M";
fi
}

case "$1" in
  "symbol")
    pactl subscribe | grep --line-buffered "Event 'change' on client" | while read -r; do
      case "$(pactl get-default-sink)" in
        *Arctis_9*) echo "";;
        *bluez*) echo "";;
        "alsa_output.platform-avs_nau8825.2.HiFi__hw_avsnau8825_1__sink") echo "";;
        #*Arctis_9*) echo "";;
        *)          echo "";;
      esac
    done
    ;;
  "volume")
    pamixer --get-volume; 
    pactl subscribe \
      | grep --line-buffered "Event 'change' on sink " \
      | while read -r evt; 
#      do pamixer --get-volume | cut -d " " -f1;
      do getvolume;
    done
    ;;
# "toggle")
#    speaker_sink_id=$(pamixer --list-sinks | grep "Komplete_Audio_6" | awk '{print $1}')
#    game_sink_id=$(pamixer --list-sinks | grep "stereo-game"         | awk '{print $1}')
#    case "$(pactl get-default-sink)" in
#      *Arctis_9*) 
#        eww -c ~/.config/eww-bar update audio_sink=""
#        pactl set-default-sink $speaker_sink_id
#        ;;
#      *)
#        eww -c ~/.config/eww-bar update audio_sink=""
#        pactl set-default-sink $game_sink_id
#        ;;
#    esac
#    ;;
esac
